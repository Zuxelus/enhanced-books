package enhancedbooks.network;

import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.relauncher.Side;
import enhancedbooks.network.messages.GuiExpBookPacket;

public class ChannelHandler
{
    public static SimpleNetworkWrapper network = NetworkRegistry.INSTANCE.newSimpleChannel("EnhancedBooks");
    public static void init()
    {
    	network.registerMessage(GuiExpBookPacket.class, GuiExpBookPacket.class, 1, Side.SERVER);
//    	network.registerMessage(PacketSensor.class, PacketSensor.class, 2, Side.CLIENT);
    }
}