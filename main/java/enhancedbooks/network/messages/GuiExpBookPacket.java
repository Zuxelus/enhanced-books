package enhancedbooks.network.messages;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.server.S1FPacketSetExperience;
import io.netty.buffer.ByteBuf;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import enhancedbooks.Utils;

public class GuiExpBookPacket implements IMessage, IMessageHandler<GuiExpBookPacket, IMessage>
{
	private int data;
	
	public GuiExpBookPacket() {}
	
	public GuiExpBookPacket(int data)
	{
		this.data = data;
	}

	@Override
	public void fromBytes(ByteBuf buf)
	{
		data = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		buf.writeInt(data);
	}
	
    @Override
    public IMessage onMessage(GuiExpBookPacket message, MessageContext ctx)
    {
        Utils.log("ServerPacketHandler: Player pressed button " + message.data);
        handleExpBookGuiButtonPressed(message.data, ctx.getServerHandler().playerEntity);
    	return null;
    }
    
    private void handleExpBookGuiButtonPressed(int buttonPressed, EntityPlayerMP player)
    {
        if (!player.isDead && !player.capabilities.isCreativeMode)
        {
            ItemStack item = player.inventory.getCurrentItem();

            if (buttonPressed == 1 && player.experienceLevel >= 1 && item.getItemDamage() >= 1)
            {
                Utils.log("ServerPacketHandler: Added one level to book");
                item.damageItem(-1, player);
                --player.experienceLevel;
            }

            if (buttonPressed == 2)
            {
                while (player.experienceLevel >= 1 && item.getItemDamage() >= 1)
                {
                    item.damageItem(-1, player);
                    --player.experienceLevel;
                }

                Utils.log("ServerPacketHandler: Added all levels to book");
            }

            if (buttonPressed == -1 && item.getItemDamage() < item.getMaxDamage())
            {
                item.damageItem(1, player);
                ++player.experienceLevel;
                Utils.log("ServerPacketHandler: Subtracted one level");
            }

            if (buttonPressed == -2)
            {
                while (item.getItemDamage() < item.getMaxDamage())
                {
                    item.damageItem(1, player);
                    ++player.experienceLevel;
                }

                Utils.log("ServerPacketHandler: Subtracted all levels");
            }

            player.playerNetServerHandler.sendPacket(new S1FPacketSetExperience(player.experience, player.experienceTotal, player.experienceLevel));
        }
    }
}