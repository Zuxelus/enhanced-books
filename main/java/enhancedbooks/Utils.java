package enhancedbooks;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.relauncher.Side;
import enhancedbooks.blocks.BlockStorageShelf;
import enhancedbooks.entities.EntityFlyingBook;
import enhancedbooks.items.ItemAtlas;
import enhancedbooks.items.ItemHollowBook;
import enhancedbooks.items.ItemXPBook;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBook;

public class Utils
{
    public static boolean isDebug()
    {
        return true;
    }

    public static void registerEntityEgg(Class <? extends Entity > entity, int primaryColor, int secondaryColor)
    {
        int id = 300;
        EntityList.IDtoClassMapping.put(id, entity);
        EntityList.entityEggs.put(id, new EntityList.EntityEggInfo(id, primaryColor, secondaryColor));
    }

    public static void log(String toLog)
    {
        if (isDebug())
        {
            EnhancedBooks.logger.info(toLog);
        }
    }
}
