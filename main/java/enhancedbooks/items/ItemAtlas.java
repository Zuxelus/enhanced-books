package enhancedbooks.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBook;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.world.World;
import cpw.mods.fml.common.registry.GameRegistry;
import enhancedbooks.EnhancedBooks;

public class ItemAtlas extends ItemBook
{
    public ItemAtlas(String name)
    {
        super();
		setUnlocalizedName(name);
        this.setCreativeTab(CreativeTabs.tabDecorations);
        GameRegistry.registerItem(this, name);
    }

    @Override
    public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer player)
    {
        if (!world.isRemote)
        {
            player.openGui(EnhancedBooks.instance, 2, world, 0, 0, 0);
        }

        return itemStack;
    }

    @Override
    public boolean getShareTag()
    {
        return true;
    }

    public static boolean validBookTagPages(NBTTagCompound par0NBTTagCompound)
    {
        if (par0NBTTagCompound == null)
        {
            return false;
        }
        else if (!par0NBTTagCompound.hasKey("pages"))
        {
            return false;
        }
        else
        {
            NBTTagList var1 = (NBTTagList)par0NBTTagCompound.getTag("pages");

            for (int var2 = 0; var2 < var1.tagCount(); ++var2)
            {
            	NBTTagCompound var3 = var1.getCompoundTagAt(var2);

/*                if (var3.data == null) // TODO
                {
                    return false;
                }

                if (var3.data.length() > 256)
                {
                    return false;
                }*/
            }

            return true;
        }
    }
}
