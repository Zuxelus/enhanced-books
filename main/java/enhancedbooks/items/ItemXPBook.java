package enhancedbooks.items;

import java.util.List;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBook;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import enhancedbooks.EnhancedBooks;

public class ItemXPBook extends ItemBook
{
    public ItemXPBook(String name)
    {
        super();
        this.setMaxStackSize(1);
        this.setCreativeTab(CreativeTabs.tabMisc);
        this.setUnlocalizedName("xpBook");
        this.setMaxDamage(50);
        this.setTextureName("enhancedbooks:XPBook");
        GameRegistry.registerItem(this, name);
    }

    @SideOnly(Side.CLIENT)
    @Override
    public void addInformation(ItemStack itemStack, EntityPlayer par2EntityPlayer, List descriptionList, boolean par4)
    {
        descriptionList.add("Level: " + (itemStack.getMaxDamage() - itemStack.getItemDamage()));
    }

    @Override
    public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer entityPlayer)
    {
        entityPlayer.openGui(EnhancedBooks.instance, 1, world, (int)entityPlayer.posX, (int)entityPlayer.posY, (int)entityPlayer.posZ);
        return itemStack;
    }

    @SideOnly(Side.CLIENT)
    @Override
    public boolean hasEffect(ItemStack itemStack)
    {
        return itemStack.getItemDamage() != itemStack.getMaxDamage();
    }
}
