package enhancedbooks.items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemBook;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import cpw.mods.fml.common.registry.GameRegistry;
import enhancedbooks.EnhancedBooks;
import enhancedbooks.inventory.InventoryHollowBook;

public class ItemHollowBook extends ItemBook
{
    public ItemHollowBook(String name)
    {
        super();
        this.setMaxStackSize(1);
        this.setCreativeTab(CreativeTabs.tabMisc);
        this.setUnlocalizedName(name);
        this.setMaxDamage(50);
        this.setTextureName("enhancedbooks:HollowBook");
        GameRegistry.registerItem(this, name);
    }

    @Override
    public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer entityPlayer)
    {
        entityPlayer.openGui(EnhancedBooks.instance, 2, world, (int)entityPlayer.posX, (int)entityPlayer.posY, (int)entityPlayer.posZ);
        return itemStack;
    }

    public static IInventory getHollowBookInv(EntityPlayer player)
    {
        InventoryHollowBook inventoryHollowBook = null;
        ItemStack hollowBook = player.getCurrentEquippedItem();

        if (hollowBook != null && hollowBook.getItem() instanceof ItemHollowBook)
        {
            inventoryHollowBook = new InventoryHollowBook(player, hollowBook);
        }

        return inventoryHollowBook;
    }
}
