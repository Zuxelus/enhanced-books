package enhancedbooks.blocks;

import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import enhancedbooks.EnhancedBooks;
import enhancedbooks.tileentities.TileEntityStorageShelf;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockStorageShelf extends BlockContainer
{
	public static int storageShelfModelID = RenderingRegistry.getNextAvailableRenderId();

	public BlockStorageShelf()
	{
		super(Material.wood);
		this.setBlockName("StorageShelf");
		this.setHardness(1.5F);
		this.setResistance(2.0F);
		this.setStepSound(Block.soundTypeWood);
		this.setCreativeTab(CreativeTabs.tabDecorations);
		this.setBlockTextureName("enhancedbooks:StorageShelf");
		GameRegistry.registerBlock(this, "StorageShelf");
	}

	@SideOnly(Side.CLIENT)
	@Override
	public IIcon getIcon(int par1, int par2)
	{
		return Blocks.planks.getIcon(par1, par2);
	}

	@Override
	public float getEnchantPowerBonus(World world, int x, int y, int z)
	{
		int power = 0;
		boolean[] filledSlots = ((TileEntityStorageShelf)world.getTileEntity(x, y, z)).getFilledSlots();

		for (int i = 0; i < 8; ++i)
		{
			power += filledSlots[i] ? 1 : 0;
		}

		return (float)power / 3.0F;
	}

	@Override
	public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase placer, ItemStack itemStack)
	{
		int rot = MathHelper.floor_double((double)(placer.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;

		if (rot == 0)
		{
			((TileEntityStorageShelf)((TileEntityStorageShelf)world.getTileEntity(x, y, z))).setDirection(2);
		}

		if (rot == 1)
		{
			((TileEntityStorageShelf)((TileEntityStorageShelf)world.getTileEntity(x, y, z))).setDirection(5);
		}

		if (rot == 2)
		{
			((TileEntityStorageShelf)((TileEntityStorageShelf)world.getTileEntity(x, y, z))).setDirection(3);
		}

		if (rot == 3)
		{
			((TileEntityStorageShelf)((TileEntityStorageShelf)world.getTileEntity(x, y, z))).setDirection(4);
		}
	}

	@SideOnly(Side.CLIENT)
	@Override
	public int getRenderType()
	{
		return storageShelfModelID;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public boolean isOpaqueCube()
	{
		return false;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public boolean renderAsNormalBlock()
	{
		return false;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public boolean shouldSideBeRendered(IBlockAccess par1IBlockAccess, int par2, int par3, int par4, int par5)
	{
		return true;
	}

	@Override
	public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int idk, float what, float these, float are)
	{
		TileEntity tileEntity = world.getTileEntity(x, y, z);

		if (tileEntity == null || player.isSneaking())
		{
			return false;
		}
		player.openGui(EnhancedBooks.instance, 0, world, x, y, z);
		return true;
	}

	@Override
	public void breakBlock(World world, int x, int y, int z, Block par5, int par6)
	{
		dropItems(world, x, y, z);
		super.breakBlock(world, x, y, z, par5, par6);
	}

	private void dropItems(World world, int x, int y, int z)
	{
		Random rand = new Random();
		TileEntity tileEntity = world.getTileEntity(x, y, z);

		if (tileEntity instanceof IInventory)
		{
			IInventory inventory = (IInventory)tileEntity;

			for (int i = 0; i < inventory.getSizeInventory(); ++i)
			{
				ItemStack item = inventory.getStackInSlot(i);

				if (item != null && item.stackSize > 0)
				{
					float rx = rand.nextFloat() * 0.8F + 0.1F;
					float ry = rand.nextFloat() * 0.8F + 0.1F;
					float rz = rand.nextFloat() * 0.8F + 0.1F;
					EntityItem entityItem = new EntityItem(world, x + rx, y + ry, z + rz, item);

					if (item.hasTagCompound())
					{
						entityItem.getEntityItem().setTagCompound((NBTTagCompound)item.getTagCompound().copy());
					}

					float factor = 0.05F;
					entityItem.motionX = rand.nextGaussian() * factor;
					entityItem.motionY = rand.nextGaussian() * factor + 0.2F;
					entityItem.motionZ = rand.nextGaussian() * factor;
					world.spawnEntityInWorld(entityItem);
					item.stackSize = 0;
				}
			}
		}
	}

	@Override
	public TileEntity createNewTileEntity(World world, int par2)
	{
		return new TileEntityStorageShelf();
	}
}
