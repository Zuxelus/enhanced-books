package enhancedbooks;

import cpw.mods.fml.client.event.ConfigChangedEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBook;
import net.minecraftforge.common.config.Configuration;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

import enhancedbooks.blocks.BlockStorageShelf;
import enhancedbooks.items.ItemAtlas;
import enhancedbooks.items.ItemHollowBook;
import enhancedbooks.items.ItemXPBook;

public class ConfigurationHandler
{
	public Configuration configuration;
	
    public static boolean bookshelvesEnabled;
    public static boolean xpBooksEnabled;
    public static boolean oldRecipe;
    private static ArrayList<String> bookshelfAllowedItems;

	public void init(File configFile)
	{
		if (configuration == null)
		{
			configuration = new Configuration(configFile);
		}
		loadConfiguration();
	}

	private void loadConfiguration()
	{
		try
		{
	        bookshelvesEnabled = configuration.get("general", "BookshelvesEnabled", true).getBoolean(true);
	        xpBooksEnabled = configuration.get("general", "Enabled", true).getBoolean(true);
	        oldRecipe = configuration.get("general", "OldRecipe", false).getBoolean(false);
	        bookshelfAllowedItems = new ArrayList(Arrays.asList(configuration.get("general", "AllowedItems", "paper,map,emptyMap,book,writingBook,writtenBook,enchantedBook,xpBook,myst.agebook,myst.linkbook,myst.notebook,myst.page,ItemThaumonomicon,ItemResearchNotes,planBlank,planFull,blueprintItem,ccprintout,spellparchment,spellbook,spellrecipescroll,bookairaffinity,bookarcaneaffinity,bookclearaffinity,bookearthaffinity,bookenderaffinity,bookfireaffinity,bookiceaffinity,booklifeaffinity,booklightningaffinity,bookmagmaaffinity,bookplantaffinity,bookwateraffinity,spellgrowth,spellfirebolt,spellgust,spelljump,spellearthshift,spelldig,spellaqua,spellfurnacetouch,spellarcanebolt,spellfrozenpath,spelllightningbolt,spellwaterbreathing,spellflight,spellfeatherfall,spellhaste,spellhealself,spelltruesight,spellfirerune,spellshockwave,spelltelekinesis,spellmark,spellrecall,spelldivineintervention,spellenderintervention,spellblink,spellregeneration,spellhealother,spellregenerateother,spelllifetap,spellfireburst,spellsummonearthgolem,spellsummonskeleton,spellsummonfireelemental,spellsummonlich,spellmagelight,spellcallrain,spellfireball,spellguardianarcanebolt,spellsummonbattlechicken,spellfireshield,spellwatershield,spellsummondryad,spellmagicshield,spelllightningstorm,spellmeteorjump,spellchainlightning,spelldispel,spelldebug,spellcharm,spellarcticwind,spellfrostbolt,spellicerune,spellearthshiftdown,spellsenseenergy,spelllifedrain,spellmanadrain,spellchronoanchor,spelltransplace,spelldigii,spellinvisibility,spellnightvision,spellinsectswarm,spelldrainingpoison,spelldrown,spellenderbolt,spellvortex,spellchannelessence,spellclarivoyance,spelldesertcurse,spellentangle,spellparasiticseed,spellwaterygrave,spellwindtalons,spellskybreath,spelldeathkiss,spellscarletflames,spellwizardsautumn,spellhandspasms,spellsevenleaguestride,spellsturdyvine,spellprotectingwinds,spellsummonshadow,spellhealingring,spellswiftswim,spellreflect,spellagility,spellaccelerate,spellwallofthorns,spellparchingwind,emptyMagicMap,emptyMazeMap,emptyOreMap,magicMap,mazeMap,oreMap,wirelessmap", "Insert the incode-name of whatever you want to be usable in bookshelves").getString().split(",")));
		}
		catch (Exception e)
		{
			EnhancedBooks.logger.error("Mod has a problem loading it's configuration", e);
		}
		finally
		{
			if (configuration.hasChanged())
			{
				configuration.save();
			}
		}
	}

	public void save()
	{
		if (configuration.hasChanged())
		{
			configuration.save();
		}
	}

	@SubscribeEvent
	public void onConfigurationChangedEvent(ConfigChangedEvent.OnConfigChangedEvent event)
	{
		if (event.modID.equalsIgnoreCase("EnhancedBooks"))
		{
			loadConfiguration();
		}
	}
	
    public static boolean isBook(Item item)
    {
        return bookshelfAllowedItems.contains(item.getUnlocalizedName().substring(5, item.getUnlocalizedName().length())) || bookshelfAllowedItems.contains(String.valueOf(Item.getIdFromItem(item))) || item instanceof ItemBook;
    }
}