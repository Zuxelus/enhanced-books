package enhancedbooks;

import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapedOreRecipe;

import org.apache.logging.log4j.Logger;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import enhancedbooks.blocks.BlockStorageShelf;
import enhancedbooks.entities.EntityFlyingBook;
import enhancedbooks.items.*;
import enhancedbooks.network.ChannelHandler;

@Mod(modid = "EnhancedBooks", name = "Enhanced Books", version = "1.7.10.01", guiFactory = "enhancedbooks.client.gui.GuiFactory")
public class EnhancedBooks
{
    @Instance("EnhancedBooks")
    public static EnhancedBooks instance;
    @SidedProxy(clientSide = "enhancedbooks.client.ClientProxy", serverSide = "enhancedbooks.CommonProxy")
    public static CommonProxy proxy;
    
    public static ConfigurationHandler config;
    public static Logger logger;
    
    public static BlockStorageShelf blockStorageShelf;
    public static ItemXPBook itemXPBook;
    public static ItemHollowBook itemHollowBook;
    public static ItemAtlas itemAtlas;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
		logger = event.getModLog();

		config = new ConfigurationHandler();
		FMLCommonHandler.instance().bus().register(config);
		config.init(event.getSuggestedConfigurationFile());
		
		Utils.registerEntityEgg(EntityFlyingBook.class, 0xffffff, 0x000000);
		
		ChannelHandler.init();
    }

    @EventHandler
    public void initBooks(FMLInitializationEvent event)
    {
        proxy.registerRenderInformation();
        proxy.registerEntities();
        
        //registerItems
        if (config.xpBooksEnabled)
        {
        	itemXPBook = new ItemXPBook("expBook");
        }
        itemHollowBook = new ItemHollowBook("hollowBook");
        
        proxy.registerTileEntities();
        
        // registerBlocks
        if (config.bookshelvesEnabled)
        {
        	blockStorageShelf = new BlockStorageShelf();
        }
        
        addRecipes();
        NetworkRegistry.INSTANCE.registerGuiHandler(instance, proxy);
    }
    
    public void addRecipes()
    {
        GameRegistry.addRecipe(new ShapedOreRecipe(blockStorageShelf, new Object[] {Boolean.valueOf(true), new Object[]{"ooo", "---", "ooo", 'o', "plankWood", '-', "slabWood"}}));
        GameRegistry.addShapelessRecipe(new ItemStack(itemHollowBook), new Object[] {new ItemStack(Items.book), new ItemStack(Items.stick)});

        if (config.oldRecipe)
        {
            GameRegistry.addShapelessRecipe(new ItemStack(itemXPBook, 1, itemXPBook.getMaxDamage()), new Object[] {new ItemStack(Items.book), new ItemStack(Items.experience_bottle)});
        }
        else
        {
            GameRegistry.addShapelessRecipe(new ItemStack(itemXPBook, 1, itemXPBook.getMaxDamage()), new Object[] {new ItemStack(Items.ender_eye), new ItemStack(Items.book), new ItemStack(Items.blaze_rod)});
        }
    }
}
