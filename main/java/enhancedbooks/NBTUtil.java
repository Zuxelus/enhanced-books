package enhancedbooks;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

public class NBTUtil
{
    private static void initNBTTagCompound(ItemStack itemStack)
    {
        if (itemStack.stackTagCompound == null)
        {
            itemStack.setTagCompound(new NBTTagCompound());
        }
    }

    public static boolean hasTag(ItemStack itemStack, String tagName)
    {
        return itemStack.stackTagCompound != null ? itemStack.stackTagCompound.hasKey(tagName) : false;
    }

    public static void removeTag(ItemStack itemStack, String tagName)
    {
        if (itemStack.stackTagCompound != null)
        {
            itemStack.stackTagCompound.removeTag(tagName);
        }
    }

    public static String getString(ItemStack itemStack, String tagName)
    {
        initNBTTagCompound(itemStack);

        if (!itemStack.stackTagCompound.hasKey(tagName))
        {
            setString(itemStack, tagName, "");
        }

        return itemStack.stackTagCompound.getString(tagName);
    }

    public static void setString(ItemStack itemStack, String tagName, String tagValue)
    {
        initNBTTagCompound(itemStack);
        itemStack.stackTagCompound.setString(tagName, tagValue);
    }

    public static Boolean getBoolean(ItemStack itemStack, String tagName)
    {
        initNBTTagCompound(itemStack);

        if (!itemStack.stackTagCompound.hasKey(tagName))
        {
            setBoolean(itemStack, tagName, false);
        }

        return itemStack.stackTagCompound.getBoolean(tagName);
    }

    public static void setBoolean(ItemStack itemStack, String tagName, boolean tagValue)
    {
        initNBTTagCompound(itemStack);
        itemStack.stackTagCompound.setBoolean(tagName, tagValue);
    }

    public static Byte getByte(ItemStack itemStack, String tagName)
    {
        initNBTTagCompound(itemStack);

        if (!itemStack.stackTagCompound.hasKey(tagName))
        {
            setByte(itemStack, tagName, (byte)0);
        }

        return itemStack.stackTagCompound.getByte(tagName);
    }

    public static void setByte(ItemStack itemStack, String tagName, byte tagValue)
    {
        initNBTTagCompound(itemStack);
        itemStack.stackTagCompound.setByte(tagName, tagValue);
    }

    public static Short getShort(ItemStack itemStack, String tagName)
    {
        initNBTTagCompound(itemStack);

        if (!itemStack.stackTagCompound.hasKey(tagName))
        {
            setShort(itemStack, tagName, (short)0);
        }

        return itemStack.stackTagCompound.getShort(tagName);
    }

    public static void setShort(ItemStack itemStack, String tagName, short tagValue)
    {
        initNBTTagCompound(itemStack);
        itemStack.stackTagCompound.setShort(tagName, tagValue);
    }

    public static Integer getInteger(ItemStack itemStack, String tagName)
    {
        initNBTTagCompound(itemStack);

        if (!itemStack.stackTagCompound.hasKey(tagName))
        {
            setInteger(itemStack, tagName, 0);
        }

        return itemStack.stackTagCompound.getInteger(tagName);
    }

    public static void setInteger(ItemStack itemStack, String tagName, int tagValue)
    {
        initNBTTagCompound(itemStack);
        itemStack.stackTagCompound.setInteger(tagName, tagValue);
    }

    public static Long getLong(ItemStack itemStack, String tagName)
    {
        initNBTTagCompound(itemStack);

        if (!itemStack.stackTagCompound.hasKey(tagName))
        {
            setLong(itemStack, tagName, 0L);
        }

        return itemStack.stackTagCompound.getLong(tagName);
    }

    public static void setLong(ItemStack itemStack, String tagName, long tagValue)
    {
        initNBTTagCompound(itemStack);
        itemStack.stackTagCompound.setLong(tagName, tagValue);
    }

    public static Float getFloat(ItemStack itemStack, String tagName)
    {
        initNBTTagCompound(itemStack);

        if (!itemStack.stackTagCompound.hasKey(tagName))
        {
            setFloat(itemStack, tagName, 0.0F);
        }

        return itemStack.stackTagCompound.getFloat(tagName);
    }

    public static void setFloat(ItemStack itemStack, String tagName, float tagValue)
    {
        initNBTTagCompound(itemStack);
        itemStack.stackTagCompound.setFloat(tagName, tagValue);
    }

    public static Double getDouble(ItemStack itemStack, String tagName)
    {
        initNBTTagCompound(itemStack);

        if (!itemStack.stackTagCompound.hasKey(tagName))
        {
            setDouble(itemStack, tagName, 0.0D);
        }

        return itemStack.stackTagCompound.getDouble(tagName);
    }

    public static void setDouble(ItemStack itemStack, String tagName, double tagValue)
    {
        initNBTTagCompound(itemStack);
        itemStack.stackTagCompound.setDouble(tagName, tagValue);
    }

    public static NBTTagCompound getCompoundTag(ItemStack itemStack, String tagName)
    {
        initNBTTagCompound(itemStack);

        if (!itemStack.stackTagCompound.hasKey(tagName))
        {
            itemStack.stackTagCompound.setTag(tagName, new NBTTagCompound());
        }

        return itemStack.stackTagCompound.getCompoundTag(tagName);
    }

    public static void setCompoundTag(ItemStack itemStack, String tagName, NBTTagCompound tagValue)
    {
        initNBTTagCompound(itemStack);
        itemStack.stackTagCompound.setTag(tagName, tagValue);
    }
}
