package enhancedbooks;

import cpw.mods.fml.common.network.IGuiHandler;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import enhancedbooks.entities.EntityFlyingBook;
import enhancedbooks.inventory.ContainerHollowBook;
import enhancedbooks.inventory.ContainerStorageShelf;
import enhancedbooks.items.ItemHollowBook;
import enhancedbooks.tileentities.TileEntityStorageShelf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraftforge.oredict.ShapedOreRecipe;

public class CommonProxy implements IGuiHandler
{
	@Override
    public Object getServerGuiElement(int id, EntityPlayer player, World world, int x, int y, int z)
    {
        if (id == 0)
        {
            TileEntity tileEntity = world.getTileEntity(x, y, z);

            if (tileEntity instanceof TileEntityStorageShelf)
            {
                return new ContainerStorageShelf(player.inventory, (TileEntityStorageShelf)tileEntity);
            }
        }

        return id == 2 ? new ContainerHollowBook(player.inventory, ItemHollowBook.getHollowBookInv(player), player.inventory.getCurrentItem()) : null;
    }

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
		// null on server
		return null;
	}

    public void registerRenderInformation() {}

    public void registerTileEntities()
    {
        GameRegistry.registerTileEntity(TileEntityStorageShelf.class, "TileStorageShelf");
    }

    public void registerEntities()
    {
        EntityRegistry.registerModEntity(EntityFlyingBook.class, "FlyingBook", 1, EnhancedBooks.instance, 80, 3, true);
    }
}
