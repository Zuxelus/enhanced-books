package enhancedbooks.inventory;

import enhancedbooks.EnhancedBooks;
import enhancedbooks.Utils;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class SlotStorageShelf extends Slot
{
    final ContainerStorageShelf linkedContainer;

    public SlotStorageShelf(ContainerStorageShelf bookshelf, IInventory par2IInventory, int slotNumber, int posX, int posY)
    {
        super(par2IInventory, slotNumber, posX, posY);
        this.linkedContainer = bookshelf;
    }

    @Override
    public boolean isItemValid(ItemStack par1ItemStack)
    {
        return isBook(par1ItemStack);
    }

    @Override
    public int getSlotStackLimit()
    {
        return 1;
    }

    public static boolean isBook(ItemStack item)
    {
        Utils.log(item.getUnlocalizedName());
        return item != null && EnhancedBooks.config.isBook(item.getItem());
    }
}
