package enhancedbooks.inventory;

import enhancedbooks.items.ItemHollowBook;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

public class ContainerHollowBook extends Container
{
    private ItemStack openedHollowBook;

    public ContainerHollowBook(IInventory playerInventory, IInventory hollowBookInventory, ItemStack hollowBook)
    {
        hollowBookInventory.openInventory();
        addSlotToContainer(new Slot(hollowBookInventory, 0, 79, 32));
        bindPlayerInventory(playerInventory);
        openedHollowBook = hollowBook;
    }

    protected void bindPlayerInventory(IInventory inventoryPlayer)
    {
        for (int i = 0; i < 3; ++i)
        {
            for (int j = 0; j < 9; ++j)
            {
                this.addSlotToContainer(new Slot(inventoryPlayer, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }

        for (int i = 0; i < 9; ++i)
        {
            this.addSlotToContainer(new Slot(inventoryPlayer, i, 8 + i * 18, 142));
        }
    }

    @Override
    public boolean canInteractWith(EntityPlayer player)
    {
        return player.getCurrentEquippedItem() != null && player.getCurrentEquippedItem().isItemEqual(openedHollowBook);
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int slotPos)
    {
        ItemStack returnStack = null;
        Slot slot = (Slot)this.inventorySlots.get(slotPos);

        if (slot != null && slot.getHasStack())
        {
            ItemStack itemStack = slot.getStack();

            if (itemStack.getItem() instanceof ItemHollowBook)
            {
                return returnStack;
            }

            returnStack = itemStack.copy();

            if (slotPos < 1)
            {
                if (!this.mergeItemStack(itemStack, 1, inventorySlots.size(), true))
                {
                    return null;
                }
            }
            else if (!this.mergeItemStack(itemStack, 0, 9, false))
            {
                return null;
            }

            if (itemStack.stackSize == 0)
            {
                slot.putStack(null);
            }
            else
            {
                slot.onSlotChanged();
            }
        }

        return returnStack;
    }
}
