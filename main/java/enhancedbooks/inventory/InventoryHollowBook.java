package enhancedbooks.inventory;

import enhancedbooks.NBTUtil;
import enhancedbooks.items.ItemHollowBook;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.InventoryBasic;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraftforge.common.util.Constants;

public class InventoryHollowBook extends InventoryBasic
{
    private String inventoryTitle;
    private EntityPlayer playerEntity;
    private ItemStack originalIS;
    private boolean reading = false;

    public InventoryHollowBook(EntityPlayer player, ItemStack is)
    {
        super("", false, getInventorySize(is));
        playerEntity = player;
        originalIS = is;

        if (!hasInventory())
        {
            createInventory();
        }

        loadInventory();
    }

    @Override
    public void markDirty()
    {
        super.markDirty();

        if (!reading)
        {
            saveInventory();
        }
    }

    @Override
    public void openInventory()
    {
        loadInventory();
    }

    @Override
    public void closeInventory()
    {
        saveInventory();
    }

    @Override
    public String getInventoryName()
    {
        return inventoryTitle;
    }

    protected static int getInventorySize(ItemStack is)
    {
        return 1;
    }

    private boolean hasInventory()
    {
        return NBTUtil.hasTag(originalIS, "Inventory");
    }

    private void createInventory()
    {
        setInvName(originalIS.getDisplayName());
        writeToNBT();
    }

    public void setInvName(String name)
    {
        this.inventoryTitle = name;
    }

    private void setNBT()
    {
        if (playerEntity.getCurrentEquippedItem() != null && this.playerEntity.getCurrentEquippedItem().getItem() instanceof ItemHollowBook)
        {
            playerEntity.getCurrentEquippedItem().setTagCompound(originalIS.getTagCompound());
        }
    }

    public void loadInventory()
    {
        readFromNBT();
    }

    public void saveInventory()
    {
        writeToNBT();
        setNBT();
    }

    private void writeToNBT()
    {
        NBTTagCompound name = new NBTTagCompound();
        name.setString("Name", this.getInventoryName());
        NBTUtil.setCompoundTag(this.originalIS, "display", name);
        NBTTagList itemList = new NBTTagList();

        for (int inventory = 0; inventory < this.getSizeInventory(); ++inventory)
        {
            if (this.getStackInSlot(inventory) != null)
            {
                NBTTagCompound slotEntry = new NBTTagCompound();
                slotEntry.setByte("Slot", (byte)inventory);
                this.getStackInSlot(inventory).writeToNBT(slotEntry);
                itemList.appendTag(slotEntry);
            }
        }

        NBTTagCompound var5 = new NBTTagCompound();
        var5.setTag("Items", itemList);
        NBTUtil.setCompoundTag(this.originalIS, "Inventory", var5);
    }

    private void readFromNBT()
    {
        this.reading = true;

        if (NBTUtil.getCompoundTag(this.originalIS, "Inventory").hasKey("title"))
        {
            this.setInvName(NBTUtil.getCompoundTag(this.originalIS, "Inventory").getString("title"));
        }
        else
        {
            this.setInvName(NBTUtil.getCompoundTag(this.originalIS, "display").getString("Name"));
        }

        NBTTagList itemList = NBTUtil.getCompoundTag(this.originalIS, "Inventory").getTagList("Items", Constants.NBT.TAG_COMPOUND);

        for (int i = 0; i < itemList.tagCount(); ++i)
        {
            NBTTagCompound slotEntry = (NBTTagCompound)itemList.getCompoundTagAt(i);
            int j = slotEntry.getByte("Slot") & 255;

            if (j >= 0 && j < this.getSizeInventory())
            {
                this.setInventorySlotContents(j, ItemStack.loadItemStackFromNBT(slotEntry));
            }
        }

        this.reading = false;
    }
}
