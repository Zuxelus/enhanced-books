package enhancedbooks.client;

import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import enhancedbooks.CommonProxy;
import enhancedbooks.blocks.BlockStorageShelf;
import enhancedbooks.client.gui.GuiHollowBook;
import enhancedbooks.client.gui.GuiStorageShelf;
import enhancedbooks.client.gui.GuiXPBook;
import enhancedbooks.client.models.ModelFlyingBook;
import enhancedbooks.client.renderers.RenderFlyingBook;
import enhancedbooks.client.renderers.StorageShelfRenderer;
import enhancedbooks.entities.EntityFlyingBook;
import enhancedbooks.items.ItemHollowBook;
import enhancedbooks.tileentities.TileEntityStorageShelf;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import org.lwjgl.opengl.GL11;

public class ClientProxy extends CommonProxy
{
	@Override
	public Object getClientGuiElement(int id, EntityPlayer player, World world, int x, int y, int z)
	{
        if (id == 0)
        {
            TileEntity tileEntity = world.getTileEntity(x, y, z);

            if (tileEntity instanceof TileEntityStorageShelf)
            {
                return new GuiStorageShelf(player.inventory, (TileEntityStorageShelf)tileEntity);
            }
        }

        return id == 1 ? new GuiXPBook(player.inventory.getCurrentItem()) : (id == 2 ? new GuiHollowBook(player.inventory, ItemHollowBook.getHollowBookInv(player)) : null);
	}
	
	@Override
    public void registerRenderInformation()
    {
        ClientRegistry.bindTileEntitySpecialRenderer(TileEntityStorageShelf.class, new StorageShelfRenderer());
        RenderingRegistry.registerBlockHandler(BlockStorageShelf.storageShelfModelID, new StorageShelfRenderer());
        RenderingRegistry.registerEntityRenderingHandler(EntityFlyingBook.class, new RenderFlyingBook());
    }

    public static void renderStandardInvBlock(RenderBlocks renderblocks, Block block, int meta)
    {
        Tessellator tessellator = Tessellator.instance;
        GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
        tessellator.startDrawingQuads();
        tessellator.setNormal(0.0F, -1.0F, 0.0F);
        renderblocks.renderFaceYNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(0, meta));
        tessellator.draw();
        tessellator.startDrawingQuads();
        tessellator.setNormal(0.0F, 1.0F, 0.0F);
        renderblocks.renderFaceYPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(1, meta));
        tessellator.draw();
        tessellator.startDrawingQuads();
        tessellator.setNormal(0.0F, 0.0F, -1.0F);
        renderblocks.renderFaceZNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(2, meta));
        tessellator.draw();
        tessellator.startDrawingQuads();
        tessellator.setNormal(0.0F, 0.0F, 1.0F);
        renderblocks.renderFaceZPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(3, meta));
        tessellator.draw();
        tessellator.startDrawingQuads();
        tessellator.setNormal(-1.0F, 0.0F, 0.0F);
        renderblocks.renderFaceXNeg(block, 0.0D, 0.0D, 0.0D, block.getIcon(4, meta));
        tessellator.draw();
        tessellator.startDrawingQuads();
        tessellator.setNormal(1.0F, 0.0F, 0.0F);
        renderblocks.renderFaceXPos(block, 0.0D, 0.0D, 0.0D, block.getIcon(5, meta));
        tessellator.draw();
        GL11.glTranslatef(0.5F, 0.5F, 0.5F);
    }
}
