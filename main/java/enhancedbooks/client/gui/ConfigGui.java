package enhancedbooks.client.gui;

import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;
import cpw.mods.fml.client.config.GuiConfig;
import enhancedbooks.EnhancedBooks;

public class ConfigGui extends GuiConfig 
{
    public ConfigGui(GuiScreen parent) 
    {
        super(parent, new ConfigElement(EnhancedBooks.config.configuration.getCategory(Configuration.CATEGORY_GENERAL)).getChildElements(), 
        		"EnhancedBooks", false, false, GuiConfig.getAbridgedConfigPath(EnhancedBooks.config.configuration.toString()));
    }
}
