package enhancedbooks.client.gui;

import enhancedbooks.Utils;
import enhancedbooks.items.ItemXPBook;
import enhancedbooks.network.ChannelHandler;
import enhancedbooks.network.messages.GuiExpBookPacket;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class GuiXPBook extends GuiScreen
{
	private ResourceLocation texture = new ResourceLocation("enhancedbooks:textures/gui/GuiXPBook.png");
	public final int xSize = 176;
	public final int ySize = 88;
	protected int guiLeft;
	protected int guiTop;
	private GuiButton plus;
	private GuiButton minus;
	private ItemStack xpBook;

	public GuiXPBook(ItemStack xpBook)
	{
		this.xpBook = xpBook;
	}

	@Override
	public void updateScreen()
	{
		if (mc.thePlayer.isDead || xpBook == null)
		{
			this.mc.thePlayer.closeScreen();
			return;
		}
		if (mc.thePlayer.inventory.getCurrentItem().getItem() instanceof ItemXPBook)
		{
			xpBook = mc.thePlayer.inventory.getCurrentItem();
		}
		plus.enabled = mc.thePlayer.inventory.getCurrentItem().getItemDamage() > 0 && mc.thePlayer.experienceLevel > 0 && !mc.thePlayer.capabilities.isCreativeMode;
		minus.enabled = mc.thePlayer.inventory.getCurrentItem().getItemDamage() < mc.thePlayer.inventory.getCurrentItem().getMaxDamage() && !mc.thePlayer.capabilities.isCreativeMode;
	}

	@Override
	public void drawScreen(int par1, int par2, float par3)
	{
		this.mc.renderEngine.bindTexture(texture);
		int k = (width - xSize) / 2;
		int l = (height - ySize) / 2;
		drawTexturedModalRect(k, l, 0, 0, xSize, ySize);
		drawString(fontRendererObj, "Level: " + (xpBook.getMaxDamage() - xpBook.getItemDamage()), guiLeft + 64, guiTop + 20, 0xEEEEEE);

		for (int j = 0; j < this.buttonList.size(); ++j)
		{
			GuiButton guibutton = (GuiButton)this.buttonList.get(j);
			guibutton.drawButton(this.mc, par1, par2);
		}
	}

	@Override
	public void initGui()
	{
		buttonList.clear();
		guiLeft = (width - xSize) / 2;
		guiTop = (height - ySize) / 2;
		buttonList.add(plus = new GuiButton(0, guiLeft + xSize - 40, guiTop + ySize / 2, 20, 20, "+"));
		buttonList.add(minus = new GuiButton(1, guiLeft + 20, guiTop + ySize / 2, 20, 20, "-"));
		plus.enabled = mc.thePlayer.inventory.getCurrentItem().getItemDamage() > 0;
		minus.enabled = mc.thePlayer.inventory.getCurrentItem().getItemDamage() < mc.thePlayer.inventory.getCurrentItem().getMaxDamage();
	}

	@Override
	public void actionPerformed(GuiButton button)
	{
		switch (button.id)
		{
		case 0:
			if (isShiftKeyDown())
			{
				ChannelHandler.network.sendToServer(new GuiExpBookPacket(2));
			}
			else
			{
				ChannelHandler.network.sendToServer(new GuiExpBookPacket(1));
			}

			break;

		case 1:
			if (isShiftKeyDown())
			{
				ChannelHandler.network.sendToServer(new GuiExpBookPacket(-2));
			}
			else
			{
				ChannelHandler.network.sendToServer(new GuiExpBookPacket(-1));
			}
		}
	}

	@Override
	protected void keyTyped(char par1, int par2)
	{
		if (par2 == 1 || par2 == this.mc.gameSettings.keyBindInventory.getKeyCode())
		{
			this.mc.thePlayer.closeScreen();
		}
	}

	@Override
	public boolean doesGuiPauseGame()
	{
		return false;
	}
}
