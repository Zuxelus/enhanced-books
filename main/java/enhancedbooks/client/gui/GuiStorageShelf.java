package enhancedbooks.client.gui;

import enhancedbooks.inventory.ContainerStorageShelf;
import enhancedbooks.tileentities.TileEntityStorageShelf;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;

import org.lwjgl.opengl.GL11;

public class GuiStorageShelf extends GuiContainer
{
	private ResourceLocation texture = new ResourceLocation("enhancedbooks:textures/gui/GuiStorageShelf.png");

	public GuiStorageShelf(InventoryPlayer inventoryPlayer, TileEntityStorageShelf tileEntity)
	{
		super(new ContainerStorageShelf(inventoryPlayer, tileEntity));
		this.ySize = 176;
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int param1, int param2)
	{
		fontRendererObj.drawString("Bookshelf", 8, 6, 4210752);
		fontRendererObj.drawString(StatCollector.translateToLocal("container.inventory"), 8, ySize - 96 + 2, 4210752);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float par1, int par2, int par3)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.renderEngine.bindTexture(texture);
		int k = (this.width - this.xSize) / 2;
		int l = (this.height - this.ySize) / 2;
		this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
	}
}
