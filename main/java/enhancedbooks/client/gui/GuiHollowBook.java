package enhancedbooks.client.gui;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import enhancedbooks.inventory.ContainerHollowBook;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;

import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class GuiHollowBook extends GuiContainer
{
	private ResourceLocation texture = new ResourceLocation("enhancedbooks:textures/gui/GuiHollowBook.png");
	private IInventory upperInventory;
	private IInventory lowerInventory;

	public GuiHollowBook(IInventory inventoryPlayer, IInventory inventoryHollowBook)
	{
		super(new ContainerHollowBook(inventoryPlayer, inventoryHollowBook, null));
		upperInventory = inventoryHollowBook;
		lowerInventory = inventoryPlayer;
		this.ySize = 176;
	}

	@Override
	protected void drawGuiContainerForegroundLayer(int par1, int par2)
	{
		fontRendererObj.drawString(StatCollector.translateToLocal(upperInventory.getInventoryName()), 8, 6, 4210752);
		fontRendererObj.drawString(StatCollector.translateToLocal(lowerInventory.getInventoryName()), 8, ySize - 104, 4210752);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float var1, int var2, int var3)
	{
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		mc.renderEngine.bindTexture(texture);
		int k = (this.width - this.xSize) / 2;
		int l = (this.height - this.ySize) / 2;
		this.drawTexturedModalRect(k, l, 0, 0, this.xSize, this.ySize);
	}
}
