package enhancedbooks.client.renderers;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.IBlockAccess;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;
import enhancedbooks.blocks.BlockStorageShelf;
import enhancedbooks.client.models.ModelStorageShelf;
import enhancedbooks.tileentities.TileEntityStorageShelf;

public class StorageShelfRenderer extends TileEntitySpecialRenderer implements ISimpleBlockRenderingHandler
{
    ModelStorageShelf model = new ModelStorageShelf();
    TileEntityStorageShelf shelf;
    private ResourceLocation storageshelf = new ResourceLocation("enhancedbooks:textures/models/StorageModel.png");

    @Override
    public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float f)
    {
        GL11.glPushMatrix();
        GL11.glTranslatef((float) x, (float) y, (float) z);
        this.renderStorageShelf(tileEntity.getBlockMetadata(), ((TileEntityStorageShelf) tileEntity).getFilledSlots());
        GL11.glPopMatrix();
    }

    private void renderStorageShelf(int direction)
    {
        this.renderStorageShelf(direction, new boolean[8]);
    }

    private void renderStorageShelf(int direction, boolean[] filledSlots) 
    {
        GL11.glPushMatrix();

        GL11.glTranslatef(0.5F, 0.5F, 0.5F);
        GL11.glRotatef((float) (360 - (direction * 90)) + 180, 0.0F, 1.0F, 0.0F);
        GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
//        GL11.glBindTexture(GL11.GL_TEXTURE_2D, FMLClientHandler.instance().getClient().renderEngine.getTexture(Utils.getResourcesDir() + "models/StorageShelf.png"));
        Minecraft.getMinecraft().getTextureManager().bindTexture(this.storageshelf);
        model.render(0.0625F, filledSlots);
        GL11.glPopMatrix();
    }

    @Override
    public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks renderer)
    {
        GL11.glPushMatrix();
        this.renderStorageShelf(1);
        GL11.glPopMatrix();
    }
    
/*    public void renderStorageShelfAt(TileEntityStorageShelf par1TileShelf, double par2, double par4, double par6, float par8, boolean[] filledSlots)
    {
        GL11.glPushMatrix();
        GL11.glEnable(GL12.GL_RESCALE_NORMAL);
        GL11.glEnable(GL11.GL_LIGHTING);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        GL11.glTranslatef((float)par2, (float)par4 + 1.0F, (float)par6 + 1.0F);
        GL11.glScalef(1.0F, -1.0F, -1.0F);
        GL11.glTranslatef(0.5F, 0.5F, 0.5F);

        if (par1TileShelf.getWorldObj() != null)
        {
            switch (par1TileShelf.getDirection())
            {
                case 2:
                    GL11.glRotatef(0.0F, 0.0F, 1.0F, 0.0F);
                    break;

                case 3:
                    GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
                    break;

                case 4:
                    GL11.glRotatef(270.0F, 0.0F, 1.0F, 0.0F);
                    break;

                case 5:
                    GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
            }

            GL11.glDisable(GL12.GL_RESCALE_NORMAL);
        }
        else
        {
            GL11.glEnable(GL11.GL_LIGHTING);
        }

        GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
        GL11.glTranslatef(-0.5F, -0.5F, -0.5F);
        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        Minecraft.getMinecraft().getTextureManager().bindTexture(this.storageshelf);
        this.model.render(0.0625F, filledSlots);
        GL11.glPopMatrix();
    }

    @Override
    public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float f)
    {
        this.renderStorageShelfAt((TileEntityStorageShelf)tileEntity, x, y, z, f, ((TileEntityStorageShelf)tileEntity).getFilledSlots());
    }

    @Override
    public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks renderer)
    {
        GL11.glPushMatrix();
        TileEntityStorageShelf shelf = new TileEntityStorageShelf();
        shelf.setDirection(4);
        TileEntityRendererDispatcher.instance.renderTileEntityAt(shelf, 0.0D, 0.0D, 0.0D, 0.0F);
        GL11.glPopMatrix();
    }
*/
    @Override
    public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer)
    {
        return false;
    }

    @Override
    public boolean shouldRender3DInInventory(int i)
    {
        return true;
    }

    @Override
    public int getRenderId()
    {
        return BlockStorageShelf.storageShelfModelID;
    }
}
